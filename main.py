from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
import time
import json
import random
import string
import secrets
import zipfile
from database import nikeDatabase
from textnowvn import TextNow
class Nike:
    proxy = None
    def __init__(self, exec_path):
        self.exec_path = exec_path
    def driverStart(self, proxy = None):
        chrome_options = webdriver.ChromeOptions()
        if proxy != None and type(proxy) == list: #use proxy options, pass this options to WebDriver Chrome
            self.proxy = proxy
            pluginfile = 'proxy_auth_plugin.zip'
            manifest_json = """
            {
                "version": "1.0.0",
                "manifest_version": 2,
                "name": "Chrome Proxy",
                "permissions": [
                    "proxy",
                    "tabs",
                    "unlimitedStorage",
                    "storage",
                    "<all_urls>",
                    "webRequest",
                    "webRequestBlocking"
                ],
                "background": {
                    "scripts": ["background.js"]
                },
                "minimum_chrome_version":"22.0.0"
            }
            """

            background_js = """
            var config = {
                    mode: "fixed_servers",
                    rules: {
                      singleProxy: {
                        scheme: "http",
                        host: "%s",
                        port: parseInt(%s)
                      },
                      bypassList: ["localhost"]
                    }
                  };

            chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

            function callbackFn(details) {
                return {
                    authCredentials: {
                        username: "%s",
                        password: "%s"
                    }
                };
            }

            chrome.webRequest.onAuthRequired.addListener(
                        callbackFn,
                        {urls: ["<all_urls>"]},
                        ['blocking']
            );
            """ % (self.proxy[0], self.proxy[1], self.proxy[2], self.proxy[3])
            with zipfile.ZipFile(pluginfile, 'w') as zp:
                zp.writestr("manifest.json", manifest_json)
                zp.writestr("background.js", background_js)
            chrome_options.add_extension(pluginfile)
            chrome_options.add_extension("canvas.crx")
            self.driver = webdriver.Chrome(executable_path=self.exec_path, options = chrome_options)
        else:
            #chrome_options.add_extension("blockimage.crx")
            #chrome_options.add_extension("canvas.crx")
            #chrome_options.add_extension("webgl.crx")
            #chrome_options.add_extension("audio.crx")
            #chrome_options.add_extension("font.crx")
            self.driver = webdriver.Chrome(executable_path=self.exec_path, options = chrome_options)
    def driverClose(self):
        self.driver.close()
    def driverModifyKernel(self):
        return 0
    def waitElement(self, elementType, elementName, mode = 'presence', timeout = 30):
        wait = WebDriverWait(self.driver, timeout)
        try:
            if elementType == 'class':
                hElement = wait.until(expected_conditions.visibility_of_element_located((By.CLASS_NAME, elementName)))
            elif elementType == 'id':
                hElement = wait.until(expected_conditions.visibility_of_element_located((By.ID, elementName)))
            elif elementType == 'xpath':
                hElement = wait.until(expected_conditions.visibility_of_element_located((By.XPATH, elementName)))
            else:
                return 0x1
            return hElement
        except:
            return 0x0
    def clickElement(self, elementName):
        return self.waitElement('xpath', elementName).click()
    def randPassword(self):
        alphabet = string.ascii_letters + string.digits
        while True:
            password = ''.join(secrets.choice(alphabet) for i in range(20))
            if (sum(c.islower() for c in password) >= 4
                    and sum(c.isupper() for c in password) >= 4
                    and sum(c.isdigit() for c in password) >= 4):
                break
        return password
    def generateInformation(self):
        firstName = random.choice(
            ['Anh', 'An', 'Bao', 'Binh', 'Bach', 'Ban', 'Bien', 'Bong', 'Cam', 'Chinh', 'Chuong', 'Cuong', 'Cong',
             'Cao', 'Cung', 'Duong', 'Dung', 'Duyen', 'Dat', 'Dinh', 'Trung', 'Chien', 'Chu', 'Chi', 'Co', 'Dieu',
             'Duy', 'Dang', 'Do', 'Duc', 'Phong', 'Diem', 'Giang', 'Gia', 'Huong', 'Ha', 'Hai', 'Hanh', 'Hoang', 'Hong',
             'Hue', 'Hieu', 'Ho', 'Ha', 'Huynh', 'Hoan', 'Kha', 'Khanh', 'Khue', 'Kieu', 'Kiet', 'Kim', 'Ky', 'Lan',
             'Lam', 'Le', 'Lien', 'Linh', 'Loan', 'Loc', 'Luc', 'Luu', 'Ly', 'Mai', 'Man', 'Minh', 'Mong', 'Moc', 'My',
             'Ngan', 'Nghi', 'Ngoc', 'Nguyet', 'Nha', 'Nhat', 'Nhu', 'Oanh', 'Phi', 'Phung', 'Phuoc', 'Phuong', 'Phong',
             'Que', 'Quynh', 'Quan', 'Quang', 'Quyet', 'Quyen', 'Quat', 'Sao', 'Son', 'Sang', 'Song', 'Suong', 'Sa',
             'Tam', 'Thinh', 'Trang', 'Thanh', 'Thach', 'Thao', 'Thai', 'Thom', 'Thuy', 'Thu', 'Thi', 'Thien', 'Tuyen',
             'Thuc', 'Trinh', 'Truc', 'Tri', 'Tien', 'Tieu', 'Tinh', 'To', 'Toan', 'Tra', 'Tram', 'Trieu', 'Tu',
             'Tuong', 'Tuyet', 'Uyen', 'Uy', 'Van', 'Viet', 'Vinh', 'Vu', 'Vy', 'Xuan', 'Xuyen', 'Xoan', 'Xinh', 'Yen',
             'Y'])
        lastName = random.choice(
            ['Nguyen', 'Ngo', 'Phan', 'Bui', 'Tran', 'Le', 'Truong', 'Ho', 'Dung', 'Vu', 'Vo', 'Dang', 'Hoang', 'Huynh',
             'Dao', 'Luong', 'Cao', 'Pham', 'Chu', 'Ta', 'Mai', 'Quach', 'Thach', 'Ton', 'Tong', 'Vuong', 'Trinh', 'Ha',
             'Cung', 'Bach', 'Giang', 'Khuat', 'Dinh', 'Phuong', 'Thai', 'Tao', 'Hua', 'Dau', 'Ong'])
        email_provider = ['gmail.com', 'yahoo.com']
        email = ('%s@%s' % (
        (firstName + lastName + str(random.randrange(1, 99999))).lower(), random.choice(email_provider)))
        dob = (random.choice(
            ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
             "19", "20", "21", "22", "23", "24", "25", "26", "27", "28"]) + random.choice(
            ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]) + random.choice(
            ["1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992",
             "1993", "1994", "1996", "1997", "1998", "1999", "2000"]))
        data = {
            'firstName': firstName,
            'lastName': lastName,
            'email': email,
            'password': self.randPassword(),
            'dob': dob
        }
        return data
    def createCookieFile(self, cookieList):
        header = ''
        content = ''
        for c in cookieList:
            if c['domain'][0] == '.':
                subDomain = 'TRUE'
            else:
                subDomain = 'FALSE'
            if 'expiry' in c.keys():
                expiry = str(c['expiry'])
            else:
                expiry = '0'
            content += c['domain'] + '\t' + subDomain + '\t' + c['path'] + '\t' + str(
                c['httpOnly']).upper() + '\t' + expiry + '\t' + c['name'] + '\t' + c['value'] + '\n'
        fileName = int(time.time())
        f = open("cookies/" + str(fileName) + ".txt", "a")
        f.write(content)
        f.close()
    def proxy_extractInfo(self, proxy):
        return proxy.split(':')
    def antibot_send_keys(self, element, value, n=1):
        cpm = [0.169, 0.171, 0.163, 0.170, 0.156, 0.1, 0.2, 0.124, 0.144]
        listChar = list(map(''.join, zip(*[iter(value)] * n)))
        for c in listChar:
            element.send_keys(c)
            time.sleep(random.choice(cpm))
    def createAccount(self, count = 1, proxyList = None):
        self.database = nikeDatabase()
        self.database.connect("venv/database/nike.db")
        countryCode = "VN"
        for i in range(0, count):
            if proxyList != None:
                self.driverStart(proxy = self.proxy_extractInfo(proxyList[i]))
            else:
                self.driverStart()
            self.driver.get('https://nike.com/vn')
            info = self.generateInformation()
            try:
                element = 'hf_title_signin_membership'
                if self.waitElement(elementType = 'id', elementName = element):
                    self.driver.find_element_by_id("hf_title_signin_membership").click()
                    self.driver.find_element_by_xpath("//*[text()='Join Us.']").click()
                    self.driver.find_element_by_name("emailAddress").send_keys(info["email"])
                    self.driver.find_element_by_name("password").send_keys(info["password"])
                    self.driver.find_element_by_name("firstName").send_keys(info["firstName"])
                    self.driver.find_element_by_name("lastName").send_keys(info["lastName"])
                    self.driver.find_element_by_name("dateOfBirth").send_keys(info["dob"])
                    self.driver.execute_script('document.getElementsByName("country")[0].value = "%s"' % countryCode)
                    self.driver.find_element_by_xpath("//*[text()='%s']" % random.choice(["Male", "Female"])).click()
                    time.sleep(3)
                    if self.driver.execute_script("return document.getElementsByClassName(\"duplicate-email\")[0].style.display") == "block":
                        print("%s is existed" % info["email"])
                        continue
                    else: #trigger join button and store credential to database
                        try:
                            self.driver.find_element_by_class_name("nike-unite-submit-button").click()
                            self.waitElement("xpath", '//*[@id="AccountMenu"]/a/div/div/div/svg')
                            if self.driver.execute_script('return document.getElementsByClassName("icon-btn ripple")[0].getElementsByTagName("svg")[0].style.display') == 'inline-block':
                                print("%s => OK" % info["email"])
                                self.database.addAccount(nikeEmail = info['email'], nikePassword = info['password'], country = countryCode)
                            else:
                                print("Check WebDriver, cannot create %s due to network status or antibot" % info['email'])
                        except Exception as e:
                            print("Error in Join button or antibot, network")
            except Exception as e:
                print(e)
            self.driver.close()
            time.sleep(random.randint(1, 5))
        self.driver.close()
        self.database.close()
    def verifyAccount(self, username, password):
        self.driverStart()
        self.driver.get('https://nike.com/vn')
        try:
            element = 'hf_title_signin_membership'
            if self.waitElement(elementType = 'id', elementName = element):
                self.driver.find_element_by_id("hf_title_signin_membership").click()
                self.antibot_send_keys(element=self.driver.find_element_by_name("emailAddress"), value=username)
                self.antibot_send_keys(element=self.driver.find_element_by_name("password"), value=password)
                time.sleep(2)
                self.driver.find_element_by_xpath('//input[@value="SIGN IN"]').click()
                try:
                    x = 0
                    while x < 10:
                        x += 1
                        if self.waitElement("xpath",'//input[@value="Dismiss this error"]', timeout = 2):
                            time.sleep(3)
                            self.driver.find_element_by_xpath('//input[@value="Dismiss this error"]').click()
                            self.antibot_send_keys(element=self.driver.find_element_by_name("password"), value=password)
                            self.driver.find_element_by_xpath('//input[@value="SIGN IN"]').click()
                        else:
                            x = 100
                            print("Pass login antibotx")
                except:
                    print("Pass login antibot")
                finally:
                    self.waitElement("xpath", '//*[@id="AccountMenu"]/a/div/div/div/svg')
                if self.driver.execute_script(
                    'return document.getElementsByClassName("icon-btn ripple")[0].getElementsByTagName("svg")[0].style.display') == 'inline-block':
                    print("Login %s OK" % username)
                    self.driver.get('https://www.nike.com/member/settings/')
                    self.waitElement("xpath", '//button[@aria-label="Add Mobile Number"]')
                    self.driver.find_element_by_xpath('//button[@aria-label="Add Mobile Number"]').click()
                    if self.waitElement(elementType = 'xpath', elementName = '//*[@id="nike-unite-progressive-profile-view"]/header/div[2]', timeout = 5):
                        print("Verify phone box")
                        #phone_country_code = self.driver.find_element_by_xpath('/html/body/div[1]/div[1]/div/div[1]/div/div[10]/form/div[1]/div[1]/div[1]/select')
                        self.driver.execute_script('document.getElementsByClassName("country")[0].value = "VN"');
                        textnow = TextNow(username='hoangke', password='hoangke123')
                        print(textnow.loginState)
                        while textnow.loginState != True:
                            del textnow
                            textnow = TextNow(username='hoangke', password='hoangke123')
                        phone_number = False
                        while phone_number == False:
                            try:
                                time.sleep(2)
                                phone_number = textnow.getPhoneNumber("1029")
                            except:
                                pass
                        self.driver.find_element_by_xpath('//input[@data-componentname="phoneNumber"]').send_keys(phone_number)
                        self.driver.find_element_by_xpath('//input[@value="Send Code"]').click()
                        code = textnow.waitCode()
                        self.antibot_send_keys(self.driver.find_element_by_xpath('//input[@placeholder="Enter Code"]'), value = code)
                        self.driver.execute_script('document.getElementsByClassName("checkbox")[0].click()') #click checkbox
                        self.driver.find_element_by_xpath('//input[@value="CONTINUE"]').click()
                        time.sleep(2)
                        if self.waitElement(elementType='xpath', elementName='//*[@id="nike-unite-progressive-profile-view"]/header/div[2]', timeout = 5): #check this box is close or not, if close => OK
                            print("Verify failed")
                        else:
                            print("Verify ok %s" % username)
                            del textnow
                            self.driver.close()
                    else:
                        print("Cannot load verify phone box")
                        self.driverClose()
                else:
                    print("Cannot login")
                    self.driverClose()
            else:
                print("Proxy timeout or antibot")
                self.driverClose()
        except Exception as e:
            #print(textnow.)
            self.driverClose()
            print(e)
nike = Nike('/Users/r00t/Desktop/Python Data/L6/driver/chromedriver')
#proxyList = ['vn1.ermes.resigame.site:46330:U5dZUwenI9:8MRsQLfgSM']

#nike.createAccount(count = len(proxyList), proxyList = proxyList)
#nike.verifyAccount('quetruong83278@yahoo.com','5cV0UWePT0z0nfEd4wrC')
#nike.verifyAccount('campham87728@yahoo.com','P4V4nWhUn40vcPYAw8lJ')
listAccount = ['huynhgiang36582@gmail.com:ecihE2N1KKl2ViK8q7i1','datle48262@yahoo.com:m7V9j1ylY2h1Xb9R6P2f','tuyentao96643@yahoo.com:UBMx9B8iGDEC0XpmyBs4','thinhdao54408@gmail.com:Y389Xawsn33kn9JYBjMB','kytrinh54274@gmail.com:qbcqQ6Kl8JN9sLq6gu0m','trangdau54978@gmail.com:EPbx7Dgj5rXl4uPOc1AI','tieuvo82980@gmail.com:MUEiZ4Ub5tPoX7oQRj81','xuanmai84379@gmail.com:nCOA28a9lpsgJz2UAkxd','caomai64778@yahoo.com:PK3ZB0tlSEqzi0WA4sEB','songgiang19977@gmail.com:PsvbAk1O4RKMaSN87Qtu','biencung21452@gmail.com:AJZ38qbHcPRuYJtPG32m','diemhuynh10483@gmail.com:pv6TZQT7yEhO3vo91G0I','tuhua6845@yahoo.com:pZ7SCqbALJUCxa5h96op','quanghua99100@gmail.com:nog0Ve0Udz01NZ3RnO9y','haitong28009@gmail.com:SsNqW0E3li62Sx8tnTmS','khuetrinh61386@gmail.com:8TCN46FY2Jla2yIHHSa0','thunguyen85501@gmail.com:bqzyWGSE2TquVe4ce4h6','binhbui18433@gmail.com:I13eVEGWjhmg4Mp2QoDC','thanhta80792@gmail.com:wbT8ZBPGo1fssUVzN57F','linhphan78022@yahoo.com:Od2gDw8Ivc2Z5wkEheQp','khaho8895@gmail.com:N27tqOYXrTkTP7r0Nxa4','hieuthach37536@gmail.com:Al6dkQj5pa21mLtH7Ueu','quehoang3404@yahoo.com:FSZN40955aIKUTLaGnhu','trungnguyen45870@gmail.com:4w17fnNy7o1935r6OYIP','banhua96583@yahoo.com:y5RAkkHLZ1G1ZccgJ7jy','linhdung3249@gmail.com:OIKX5y02aLzA4CUO2JZf','quynhdao26772@gmail.com:eM5oR5qR01EMH8CQRHyG','ducquach85475@gmail.com:BuW421B1y3vKkHk5QHNJ','thiengiang73044@gmail.com:i4tVhL3FBL6fNvJ0BkCV','vietpham69684@gmail.com:42FAD6kfaHgvRX43Kg40','cuonghua50675@gmail.com:nVjO4H3jyQbpGY4f1zUT','phonghua29961@yahoo.com:37WlvBflf0092PwJR2Kp','bachtrinh21166@yahoo.com:v91P3B3IewycFQEojazh','dungdung80388@yahoo.com:9gS4Gvu4V0IsLqzseJl9','xuanmai18406@yahoo.com:xT4VjTFet9BHk3778nQ5','mantong1010@yahoo.com:7Lzn1qkQGgQMvpw9KUx6']
for account in listAccount:
    info = account.split(':')
    print("Starting to verify %s:%s" % (info[0], info[1]))
    nike.verifyAccount(info[0],info[1])
