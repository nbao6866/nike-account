import requests
import pickle
import os
from lxml.html import fromstring
import re
import json
import multiprocessing
import time
import random
from faker import Faker
from faker.providers import user_agent
def randUA():
    fake = Faker()
    fake.add_provider(user_agent)
    return fake.user_agent()
class TextNow():
    session = requests.Session()
    listServices = None
    loginState = False
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36'
    def __init__(self, username, password):
        self.username = username
        self.password = password
        #test
        #self.loadSession()
        #test
        self.login()
        self.getServicesPrice()
    def __del__(self):
        print('Destructor called, Employee deleted.')
    def makeRequests(self, url, headers={}, data=None):
        default_headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'user-agent': self.user_agent,
            'Cache-Control': 'no-cache',
            'Accept-Encoding' : 'gzip, deflate, br'}
        current_headers = {**headers, **default_headers}
        if data == None:
            result = self.session.get(url, headers=current_headers, allow_redirects=True).text
        else:
            result = self.session.post(url, headers=current_headers, allow_redirects=True, data=data).text
        self.saveSession()
        return result
    def deleteSession(self):
        if os.path.exists('venv/session/textnow.session'):
            os.remove('venv/session/textnow.session')
        else:
            print("The file does not exist")
    def loadSession(self):
        self.sessionPath = 'venv/session/textnow.session'
        if self.sessionPath != None and os.path.exists(self.sessionPath) == False:
            hOpen = open(self.sessionPath, 'w+')
            hOpen.close()
            if os.path.exists(self.sessionPath) == False:
                print("Cannot create session file, check the permissions of the script and current user")
        if os.stat(self.sessionPath).st_size > 0:
            print("loading previous session from %s" % self.sessionPath)
            with open(self.sessionPath, 'rb') as f:
                self.session.cookies.update(pickle.load(f))
                print("%s has been loaded" % self.sessionPath)

    def saveSession(self):
        self.sessionPath = 'venv/session/textnow.session'
        if os.path.exists(self.sessionPath) == False:
            hOpen = open(self.sessionPath, 'w+')
            hOpen.close()
        if self.sessionPath != None and os.path.exists(self.sessionPath):
            with open(self.sessionPath, 'wb') as f:
                pickle.dump(self.session.cookies.get_dict(), f)
        return 0
    def isLogin(self):
        response = self.makeRequests("https://textnow.vn/info")
        if self.username in response:
            return True
    def login(self):
        if self.isLogin():
            print("Already login [Loaded from previous session]")
            self.loginState = True
            return True
        headers = {
            'content-type' : 'application/x-www-form-urlencoded',
            'Origin' : 'https://textnow.vn',
            'Referer' : 'https://textnow.vn/login'
        }
        data = {
            'username' : 'hoangke',
            'password' : 'hoangke123'
        }
        response = self.makeRequests('https://textnow.vn/login', data = data, headers = headers)
        if "Thông tin tài khoản" not in response:
            print("Cannot login %s" % self.username)
            self.login()
            print(response)
            return False
        elif self.username in response:
            self.loginState = True
            return True
        else:
            raise Exception("Unknow error in login process, check network or target's antibot")
    def logout(self):
        response = self.makeRequests('https://textnow.vn/logout')
        return
    def getAccountInfo(self):
        response = self.makeRequests("https://textnow.vn/info")
        eTree = fromstring(response)
        username = eTree.xpath('//*[@id="sidebar"]/div/div/div[2]/a/strong/text()')
        if len(username) < 1:
            print("Cannot get username")
        balance = eTree.xpath('//*[@id="sidebar"]/div/div/div[3]/span/strong/text()')
        if len(balance) < 1:
            print("Cannot get balance")
        filtered_balance = re.findall("\d+", balance[0])
        accountInfo = {
            'username' : self.username,
            'balance' : int(filtered_balance[0]+filtered_balance[1])
        }
        self.balance = accountInfo['balance']
        return accountInfo
    def getServicesPrice(self):
        listServers = {
            's1' : {
                'status' : True,
                'link' : 'https://textnow.vn/'
            },
            's2' : {
                'status' : False,
                'link' : 'https://textnow.vn/thue-sim-online'
            },
            's3' : {
                'status' : True,
                'link' : 'https://textnow.vn/receive-sms'
            }
        }
        listServices = {}
        for server in listServers.keys():
            if listServers[server]['status']:
                listServices[server] = {}
                response = self.makeRequests(listServers[server]['link'])
                eTree = fromstring(response)
                option = eTree.xpath('//*[@id="service"]/option')
                for i in option[1:]:
                    serviceId = i.xpath("@value")[0]
                    service = i.xpath("text()")[0].replace("\r\n                                        ", "")
                    info = service.split(" - ")
                    listServices[server][info[0]] = {
                        'id' : serviceId,
                        'price' : info[1]}
        self.listServices = listServices
        return self.listServices
    def serviceInfo(self, serviceName = None):
        if self.listServices == None:
            self.getServicesPrice()
        if serviceName == None:
            return self.listServices
        else:
            result = {}
            for server in self.listServices.keys():
                if serviceName in self.listServices[server].keys():
                    result[server] = {}
                    result[server][serviceName] = self.listServices[server][serviceName]
                    print("Server %s: %s => %s" % (server, serviceName, self.listServices[server][serviceName]))
                else:
                    print("Not found %s in %s" % (serviceName, server))
            return result

    def serviceIdToName(self, serviceId):
        result = {}
        for server in self.listServices.keys():
            for service in self.listServices[server].keys():
                if self.listServices[server][service]['id'] == serviceId:
                    result['name'] = service
                    result['price'] = self.listServices[server][service]['price']
        return result
    def getPhoneNumber(self, serviceId = None):
        #create sms service request in TextNow system and retrieve the request id
        if serviceId == None:
            print("Please specify serviceId")
            print(self.listServices)
        elif serviceId != None:
            headers = {
                'Accept' : 'application/json, text/javascript, */*; q=0.01',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
                'X-Requested-With' : 'XMLHttpRequest'
            }
            network_id = random.choice(["2","3"])
            data = {
                'service_id' : serviceId,
                'network_id' : network_id
            }
            response = self.makeRequests('https://textnow.vn/v3/get_phone', headers = headers, data = data)
            if 'Sai!' in response:
                print("serviceId not found")
                return False
            elif '"status":true' in response: #JSON object
                jsonObj = json.loads(response)
                self.phoneNumber = jsonObj['data']['phone_number']
                self.sessionId = jsonObj['data']['session']
                print(self.phoneNumber, self.sessionId)
                return self.phoneNumber
            else:
                #retry only one time
                print('Retry')
                #test block code
                self.logout()
                self.login()
                self.getPhoneNumber(serviceId)
                #test block code
                print(response)
                print("Cannot get phone number")
                return False
        else:
            return False
    def waitCode(self, delay = 2, timeout = 30):
        for i in range(timeout+1):
            try:
                headers = {
                    'Accept': 'application/json, text/javascript, */*; q=0.01',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-Requested-With': 'XMLHttpRequest'
                }
                data = {
                    'require_id' : self.sessionId
                }
                response = self.makeRequests('https://textnow.vn/v3/get_code', headers = headers, data = data)
                if '{"status"' in response: #verify json
                    jsonObj = json.loads(response)
                    if jsonObj['status']:
                        print(jsonObj['message'])
                        print(re.findall("\d+", jsonObj["message"])[0])
                        return re.findall("\d+", jsonObj["message"])[0]
                    else:
                        print("Code still not arrived yet (session_id = %s, phone = %s)" % (self.sessionId, self.phoneNumber))
                time.sleep(delay)
            except:
                pass
textnow = TextNow(username = 'hoangke', password = 'hoangke123')
#textnow.getPhoneNumber(serviceId = 1029)
#textnow.waitCode()