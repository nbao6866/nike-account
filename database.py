import sqlite3
from sqlite3 import Error
class nikeDatabase():
    def connect(self, db_file):
        conn = None
        try:
            conn = sqlite3.connect(db_file)
            print('SQLite '+sqlite3.version)
        except Error as e:
            print(e)
        finally:
            try:
                self.conn = conn
            except Error as e:
                print(e)
    def close(self):
        try:
            self.conn.close()
            print("Connection has been close")
        except:
            raise SystemError("Have no connection to close")
    def query(self, statement, isInsert = False, cursorReturn = False):
        if self.conn:
            flag = True
            try:
                cursor = self.conn.cursor()
                cursor.execute(statement)
                if isInsert:
                    self.conn.commit()
                    print("Commit")
            except Error as e:
                flag = False
                print(e)
            finally:
                if cursorReturn:
                    return cursor
                else:
                    return flag
        else:
            raise Exception("Database connection has not been established")
            return False
    def addAccount(self, nikeEmail, nikePassword, isVerify = False, country = 'VN'):
        if self.isAccountExist(nikeEmail) == False:
            result = self.query(statement = 'INSERT INTO nike_account (email, password, phoneverify, country, created_time) VALUES (\'%s\',\'%s\', \'%s\', \'%s\', datetime())' % (nikeEmail, nikePassword, isVerify, country), isInsert = True)
            return result
        else:
            print("%s is existed" % nikeEmail)
            return False
    def isAccountExist(self, nikeEmail):
        result = self.query("SELECT * FROM nike_account WHERE email = '%s'" % nikeEmail, cursorReturn = True).fetchall()
        if len(result) > 0:
            return True
        else:
            return False
    def verifyPhoneSuccess(self, email):
        result = self.query(statement = 'UPDATE nike_account SET phoneverify = \'True\' WHERE email = \'%s\'' % email, isInsert = True)
        return result
#db = nikeDatabase()
#db.connect('venv/database/nike.db')
#listAccount = ['lienngo12266@gmail.com','dangong81569@yahoo.com','kydao84945@gmail.com','sonnguyen89383@gmail.com','diemdinh28854@gmail.com','cambui80940@yahoo.com','trieubui72603@yahoo.com','phongbui7474@gmail.com','banchu71679@yahoo.com','phithai76908@yahoo.com','caovuong34341@gmail.com','quyenvu2005@gmail.com','thaiphuong48557@yahoo.com','linhdau7884@yahoo.com','khuevuong98305@gmail.com','phidang25696@yahoo.com','quanle68294@gmail.com','tienle90648@yahoo.com','ydung51450@gmail.com','quangha75396@yahoo.com','lebui92093@gmail.com','ydung51450@gmail.com','quangha75396@yahoo.com','banphan21525@yahoo.com','loanvuong57062@yahoo.com','yhoang96049@gmail.com','lydinh18817@yahoo.com','yenle64541@yahoo.com','thanhquach31533@yahoo.com','ngochoang53570@yahoo.com','loanong70047@gmail.com','thanhquach31533@yahoo.com','ngochoang53570@yahoo.com','thaikhuat52488@yahoo.com','hoanquach56786@gmail.com','triho91907@gmail.com','savuong74558@yahoo.com','copham36557@gmail.com','uythai77087@gmail.com','tamtao86116@gmail.com']
#for account in listAccount:
    #print(db.verifyPhoneSuccess(account))